module Entstudio
  module ApplicationHelper
    
    def getstatuslist(withempty=nil)
      result = []
      if not withempty.nil?
        Rails.logger.debug "not empty"
        result.push ["", ""]
      else
        Rails.logger.debug "is empty"
      end
      
      result += [
        ["Active", 1], ["Inactive", 0]
      ]
      result
    end

    def getstatushash
      result = {}
      list = getstatuslist
      list.each do | row |
        result[row[1]] = row[0]
      end
      result
    end

    def loadform
      if @statushash.nil?
        @statushash = getstatushash
        @status_options = getstatuslist
      end
    end
    
  end
end
