module Entstudio
  module Db
    class Where
      
      def initialize
        @aCondition = []
      end
      
      def add(condition, condtype="and")
        result = _add(condition, condtype)
        
        #logger.debug "adding cond"
        #logger.debug result

        @aCondition.push result
        @result = nil
      end
      
      def _add(condition, condtype="and")
        condtype = "and" if condtype.nil?
        if not ["and", "or"].include?(condtype.downcase)
          raise "Invalid type: " + condtype
        end

        if condition[:condition].nil? or condition[:condition].empty?
          raise "Missing condition"
        end
        
        if condition[:condition].kind_of?(Array)
          oCondition = {:condition => [] }
          condition[:condition].each do |innercond| 
            log innercond
            innertype = innercond[:type]
            oCondition[:condition].push _add(innercond, innertype)
          end
        else
          oCondition = condition.clone
        end
        
        result = {:type => condtype}
        result = result.merge(oCondition)
        
        log result
        result
      end
      
      def _to_a(condition)
        aSQL = []
        aParam = []
        if condition[:condition].kind_of?(Array)
          condition[:condition].each do |innercond|
            if not aSQL.empty?
              aSQL.push innercond[:type]
            end
            condresult = _to_a(innercond)
            aSQL.push "(" + condresult[:sql] + ")"
            aParam += condresult[:value]
          end
          
        else
          aSQL.push condition[:condition]
          if not condition[:value].nil? and not condition[:value].empty?
            aParam.push condition[:value]
          end
        end
        
        {:sql => aSQL.join(" "), :value => aParam}
      end
      
      # to array of condition for where
      def to_a
        if @result.nil?
          aSQL = []
          aParam = []
          @aCondition.each do |condition|
            if not aSQL.empty?
              aSQL.push condition[:type]
            end
            
            condresult = _to_a(condition)
            if condition[:condition].kind_of?(Array)
              aSQL.push "(" + condresult[:sql] + ")"
            else
              aSQL.push condresult[:sql]
            end
            log "joining: " + condresult[:value].to_s
            aParam += condresult[:value]
          end
          sSQL = aSQL.join(" ")
          @result = [sSQL] + aParam
        end
        log @result
        @result
      end
      
      def log(msg)
        Rails.logger.debug msg
      end
    end
  end
end